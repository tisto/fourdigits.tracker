from five import grok
from Products.CMFCore.utils import getToolByName
from Acquisition import aq_inner
from fourdigits.tracker.privateticket import IPrivateTicket
from fourdigits.tracker.ticket import ITicket
from fourdigits.tracker.ticketresponse import ITicketResponse

from plone.directives import dexterity
from plone.z3cform import z2
from z3c.form.interfaces import IFormLayer
from plone.z3cform.fieldsets import extensible
from z3c.form import form, field, button, browser
from zope.interface import alsoProvides
from plone.z3cform.interfaces import IWrappedForm
from z3c.form.browser.radio import RadioFieldWidget
from fourdigits.tracker import MessageFactory as _
from plone.dexterity.utils import createContentInContainer
from zope import schema
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary
from AccessControl import getSecurityManager

from fourdigits.tracker.ticket import getManagers

grok.templatedir("templates")


@grok.provider(IContextSourceBinder)
def reviewstate(context):
    """ Return the state of the time entry
    """
    terms = []
    wtool = getToolByName(context, "portal_workflow")

    #Adding the don't change option
    terms.append(SimpleVocabulary.createTerm('dontchange',
                                             'dontchange',
                                             'Do not change',
                                             ))

    actions = wtool.getTransitionsFor(context)
    for x in actions or []:
        terms.append(SimpleVocabulary.createTerm(x['id'], str(x['id']),
                                                 x['title']))

    # check if ticket or private ticket
    if IPrivateTicket.providedBy(context):
        curState = wtool.getStatusOf("privateticket_workflow", context)
    elif ITicket.providedBy(context):
        curState = wtool.getStatusOf("ticket_workflow", context)

    curStateID = curState['review_state']
    curStateTitle = wtool.getTitleForStateOnType(curStateID,
                    context.portal_type)

    terms.append(SimpleVocabulary.createTerm(curStateID,
                                             curStateID,
                                             curStateTitle,
                                             ))
    return SimpleVocabulary(terms)


class TicketResponse(extensible.ExtensibleForm, form.Form):

    ignoreContext = True  # don't use context to get widget data
    id = None
    label = _(u"Add a ticketreply")

    description = schema.Text(__name__="description",
                              title=u"Description",
                              required=False)

    review_state = schema.Choice(__name__="review_state",
                                 title=u"State",
                                 source=reviewstate,
                                 )

    responsiblemanager = schema.Choice(__name__="responsiblemanager",
                                       title=_(u"Responsible manager"),
                                       source=getManagers,
                                       )

    fields = field.Fields(description)
    fields += field.Fields(review_state)
    fields += field.Fields(responsiblemanager)
    fields += field.Fields(ITicketResponse).omit('responsetype')
    fields['review_state'].widgetFactory = RadioFieldWidget
    fields['responsiblemanager'].widgetFactory = RadioFieldWidget

    def updateFields(self):
        super(TicketResponse, self).updateFields()
        if not self.context.privateDataAllowed():
            self.fields = self.fields.omit('time_spent', 'responsiblemanager')

        #self.fields['time_spent'].write_permission('cmf.ManagePortal')
        #self.fields['time_spent'].read_permission('cmf.ManagePortal')

    def updateWidgets(self):
        if not 'form.widgets.responsiblemanager' in self.request.form:
            self.request.form['form.widgets.responsiblemanager'] = \
                self.context.responsiblemanager
        if not 'form.widgets.review_state' in self.request.form:
            pw = getToolByName(self.context, 'portal_workflow')

            # check if ticket or private ticket
            if IPrivateTicket.providedBy(self.context):
                curState = pw.getStatusOf("privateticket_workflow",
                                          self.context)
            elif ITicket.providedBy(self.context):
                curState = pw.getStatusOf("ticket_workflow", self.context)

            self.request.form['form.widgets.review_state'] = \
                curState['review_state']

        super(TicketResponse, self).updateWidgets()
        # Widgets
        self.widgets['description'].addClass("autoresize")
        self.widgets['description'].rows = 10
        if self.context.privateDataAllowed():
            self.widgets['time_spent'].addClass('input-time')

        #Adding do not change to the review state and make sure it's checked
        for item in self.widgets['review_state'].items:
            if item['value'] == 'dontchange':
                item['checked'] = True
            else:
                item['checked'] = False

    @button.buttonAndHandler(_(u"add_comment_button",
                             default=u"Comment"),
                             name='comment')
    def handleComment(self, action):

        data, errors = self.extractData()
        if errors:
            return

        conversation = ITicket(self.__parent__)

        time_spent = data.get('time_spent', 0)
        ticketresponse = createContentInContainer(conversation,
                                        'fourdigits.tracker.ticketresponse',
                                        checkConstraints=False)

        portal_membership = getToolByName(self.context,
                                        'portal_membership',
                                        None)
        member = portal_membership.getAuthenticatedMember()
        username = member.getUserName()
        email = member.getProperty('email')
        fullname = member.getProperty('fullname')
        if not fullname or fullname == '':
            fullname = member.getUserName()
        # memberdata is stored as utf-8 encoded strings
        elif isinstance(fullname, str):
            fullname = unicode(fullname, 'utf-8')
        if email and isinstance(email, str):
            email = unicode(email, 'utf-8')
        ticketresponse.creator = fullname
        ticketresponse.author_username = username
        ticketresponse.author_name = fullname
        ticketresponse.author_email = email
        ticketresponse.time_spent = time_spent
        ticketresponse.description = data['description']
        ticketresponse.attachments = data['attachments']

        pw = getToolByName(self.context, 'portal_workflow')
        # check if ticket or private ticket
        if IPrivateTicket.providedBy(self.context):
            curState = pw.getStatusOf("privateticket_workflow", self.context)
        elif ITicket.providedBy(self.context):
            curState = pw.getStatusOf("ticket_workflow", self.context)

        workflowchange = False
        if data['review_state'] != "dontchange":
            if not (data['review_state'] == curState['review_state']):
                current = pw.getWorkflowById('ticket_workflow').states.get(
                                conversation.portal_workflow.getInfoFor(
                                    conversation, 'review_state')).title
                workflowchange = current +\
                                ' to ' + pw.getWorkflowById(
                                                'ticket_workflow').states.get(
                                                    data['review_state']).title

        responsiblechange = None
        ticketresponse.changes = {}
        if 'responsiblemanager' in data:
            if conversation.responsiblemanager != data['responsiblemanager']:
                responsiblechange = str(conversation.responsiblemanager) +\
                                    ' to ' +\
                                        str(data['responsiblemanager'])

            conversation.responsiblemanager = data['responsiblemanager']

        if workflowchange:
            ticketresponse.changes['workflowchange'] = workflowchange

        if responsiblechange:
            ticketresponse.changes['responsiblechange'] = responsiblechange

        if data['review_state'] != "dontchange":
            if not (curState['review_state'] == data['review_state']):
                conversation.portal_workflow.doActionFor(conversation,
                                                         data['review_state'])
        conversation.time_real += time_spent

        #ticketresponse.creation_date = datetime.now()
        #ticketresponse.modification_date =  datetime.now()

        # Add a comment to the conversation
        #comment_id = conversation.addComment(ticketresponse)

        # Redirect after form submit:
        # Redirect to comment (inside a content object page)
        self.request.response.redirect(self.action + '#')


class TicketView(dexterity.DisplayForm):
    grok.context(ITicket)
    grok.require('zope2.View')

    form = TicketResponse

    def update(self):
        z2.switch_on(self, request_layer=IFormLayer)
        self.form = self.form(aq_inner(self.context), self.request)
        alsoProvides(self.form, IWrappedForm)
        self.form.update()

        self.details = self.converttext(aq_inner(self.context).details)

    def converttext(self, text):
        if text:
            trans = aq_inner(self.context).portal_transforms
            html = trans.convertTo('text/html',
                                   text,
                                   mimetype='text/text')
            return html.getData()

    @property
    def portal_catalog(self):
        return getToolByName(self.context, 'portal_catalog')

    @property
    def portal(self):
        return getToolByName(self.context, 'portal_url').getPortalObject()

    def getReplies(self):
        brains = self.portal_catalog(
                        object_provides=ITicketResponse.__identifier__,
                        path='/'.join(self.context.getPhysicalPath()),
                        sort_on='modified')
        replies = []
        for brain in brains:
            reply = brain.getObject()
            #See if there are changes present
            try:
                getattr(reply, "changes")
            #No changes, set to False
            except AttributeError:
                reply.changes = {}
                reply.changes['responsiblechange'] = False
                reply.changes['workflowchange'] = False

            # see if there is a workflowchange
            try:
                if reply.changes['workflowchange'] == '':
                    reply.changes['workflowchange'] = False
            except KeyError:
                reply.changes['workflowchange'] = False

            # see if there is a responsiblechange
            try:
                if reply.changes['responsiblechange'] == '':
                    reply.changes['responsiblechange'] = False
            except KeyError:
                reply.changes['responsiblechange'] = False

            replies.append(reply)

        return replies

    def toLocalizedTime(self, time, long_format=None, time_only=None):
        """Convert time to localized time
        """
        util = getToolByName(self.context, 'translation_service')
        return util.ulocalized_time(time, long_format, time_only, self.context,
                                    domain='plonelocales')

    def getPrivateText(self):
        privatetext = self.w['private_text']
        if privatetext.value == u'':
            return False
        else:
            return privatetext
