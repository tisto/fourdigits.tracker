import socket


class GozerbotUdpSender(object):

    def __init__(self, host, password, channel, port):
        self.config = {}
        self.config['host'] = host
        self.config['passwd'] = password
        self.config['printto'] = channel
        self.config['port'] = port

    def out(self, what):
        #Format as string so we can strip it
        what = str(what)

        #Ready the load we will send
        load = '%s %s %s' % (self.config['passwd'],
                             self.config['printto'],
                             what.strip())

        while len(load) % 16:
            load += "\0"
        else:
            data = load

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        #Send it!
        sock.sendto(data, (self.config['host'], int(self.config['port'])))

    def handleCommitPayload(self, payload):

        #Seperator
        self.out('-------------')

        #The author
        self.out(payload['authors'] + " committed to " + payload['reposurl'])

        #Post all the commits
        for commit in payload['commits']:

            #Commit message and sha1
            commitMessage = "Commited " +\
                            commit['node'] + ": '" +\
                            commit['message']

            if commit['branch'] != "":
                commitMessage += "' on branch " + commit['branch']

            self.out(commitMessage)

            #Link to changeset
            self.out(commit['linkChangeset'])

            #Files
            self.out("Files:")
            for changedFile in commit['files']:
                self.out("- " + changedFile['file'] + " " +
                         changedFile['type'])

        #Seperator
        self.out('-------------')
