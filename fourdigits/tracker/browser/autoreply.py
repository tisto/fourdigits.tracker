from Products.CMFCore.utils import getToolByName
from fourdigits.tracker.ticket import ITicket
from Products.Five import BrowserView
import re


#This class is used with an svn hook(add it on 1 line:
# wget https://*user*:*password*@yoursite.url/@@autoreply?
# revision=$TXN&author=$AUTHOR&commitmsg=$COMMIT

class Autoreply(BrowserView):

    def __call__(self):
        ticketnumber = self.getTicketNumber(self.request.get("commitmsg"))
        ticket = self.getTicket(ticketnumber)

    def getTicket(self, ticketNumber):
        portal_catalog = getToolByName(self.context, 'portal_catalog')
        query = {'object_provides': ITicket.__identifier__,
                    'id': ticketNumber}
        result = portal_catalog.search(query)

        if result:
            return result
        else:
            return False

    def getTicketNumber(self, commitmsg):
        """ Getting the ticketnumber from the commitmsg
        """
        if "#" in commitmsg:
            regex = re.compile(r"([#?])(\w+)\b")
            ticketnumber = regex.findall(commitmsg)[1]
            return ticketnumber
        else:
            return False
