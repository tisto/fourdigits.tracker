from five import grok
from zope.component import getUtility
from zope.publisher.interfaces import IPublishTraverse
from Products.CMFCore.utils import getToolByName
from fourdigits.tracker.ticket import ITicket
from Products.Five import BrowserView
from plone.registry.interfaces import IRegistry
from fourdigits.tracker.controlpanel import ITrackerSettings
import json
from fourdigits.tracker.browser.gozerbotudpsender import GozerbotUdpSender
import re
from plone.dexterity.utils import createContentInContainer


class CommitParser(BrowserView):
    grok.implements(IPublishTraverse)

    def __call__(self):
        registry = getUtility(IRegistry)
        self.settings = registry.forInterface(ITrackerSettings)

        if self.checkAuthKey():
            # Get the payload
            try:
                payload = json.loads(self.request['payload'])
            except KeyError:
                return "No payload detected"

            # Ready the payload
            payload = self.convertBitbucketPayload(payload)

            # Gozerbot
            if self.settings.gozerbotenabled:
                self.parseToGozerBot(payload)

            # Ticketparser
            #self.parseToTickets(payload)
        else:
            return "Unauthorized"

    def parseToGozerBot(self, payload):
        gozerbot = GozerbotUdpSender(self.settings.gozerbothost,
                                     self.settings.gozerbotpassword,
                                     self.settings.gozerbotchannel,
                                     self.settings.gozerbotport
                                     )
        gozerbot.handleCommitPayload(payload)

    def parseToTickets(self, payload):
        for commit in payload['commits']:
            ticketnr = self.getTicketNumber(commit['message'])
            if ticketnr:
                ticket = self.getTicket(ticketnr)._unrestrictedGetObject()
                ticketresponse = createContentInContainer(ticket,
                                        'fourdigits.tracker.ticketresponse',
                                        checkConstraints=False)
                ticketresponse.description = commit['message']

                #ticket.addreply(type=scmmessage,
                #commit['message'],
                #commit['linkChangeset'])

    def checkAuthKey(self):
        if self.settings.enabled:
            try:
                if self.settings.authkey == self.request['authkey']:
                    return True
            except KeyError:
                return False

        return False

    def getTicket(self, ticketNumber):
        portal_catalog = getToolByName(self.context, 'portal_catalog')
        query = {'object_provides': ITicket.__identifier__,
                    'id': ticketNumber}
        result = portal_catalog.search(query)

        if result:
            return result
        else:
            return False

    def getTicketNumber(self, commitmsg):
        """ Getting the ticketnumber from the commitmsg
        """
        if "#" in commitmsg:
            regex = re.compile(r"([#?])(\w+)\b")
            ticketnumber = regex.findall(commitmsg)[0][1]
            return ticketnumber
        else:
            return False

    def convertBitbucketPayload(self, payload):
        result = {}
        result['reposurl'] = "http://bitbucket.org" +\
            payload['repository']['absolute_url']

        result['authors'] = payload['commits'][0]['author']

        result['commits'] = []
        for commit in payload['commits']:
            ct = {}
            ct['node'] = commit['node']
            ct['message'] = commit['message']

            # Branch might be empty
            if commit['branch'] is not None:
                ct['branch'] = commit['branch']
            else:
                ct['branch'] = ""

            ct['linkChangeset'] = result['reposurl'] +\
                                  "changeset/" + commit['node']

            ct['files'] = commit['files']
            result['commits'].append(ct)

        return result
