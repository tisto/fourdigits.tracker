from five import grok
from Products.CMFCore.utils import getToolByName
from Acquisition import aq_inner
from fourdigits.tracker.tracker import ITracker
from fourdigits.tracker.ticket import ITicket
from plone.directives import dexterity
from fourdigits.tracker.browser.tickettable import TicketTable
from zope.component import getMultiAdapter
from AccessControl import getSecurityManager

grok.templatedir("templates")


class TrackerView(dexterity.DisplayForm):
    grok.context(ITracker)
    grok.require('zope2.View')

    @property
    def portal_catalog(self):
        return getToolByName(self.context, 'portal_catalog')

    @property
    def portal(self):
        return getToolByName(self.context, 'portal_url').getPortalObject()

    def getTicketTable(self, tickets):
        ticketTable = TicketTable(self.context, self.request)
        return ticketTable(tickets)

    def getReviewStates(self):
        pw = getToolByName(self.context, 'portal_workflow')
        states = pw.getWorkflowById('ticket_workflow').states.objectValues()
        return states

    def getIssuetypes(self):
        srcIssuetype = self.request.issuetype
        for issuetype in self.context.issuetypes:
            if issuetype == srcIssuetype:
                return issuetype.title()

    def ticketsForCurrentUser(self):
        portal_state = getMultiAdapter(
            (self.context, self.request), name="plone_portal_state")
        user = portal_state.member()

        email = user.getUserName()
        query = {'responsiblemanager': email,
                'sort_on': 'ModificationDate',
                'sort_order': 'descending'}

        context = aq_inner(self.context)
        result = self.portal_catalog(query,
                        object_provides=ITicket.__identifier__,
                        path='/'.join(context.getPhysicalPath()),
                        sort_on='sortable_title')
        return result

    def getTickets(self, state=False, issuetype=False):
        query = {}
        if state:
            query = {'review_state': state}
            if state == 'All':
                query = {}

        if issuetype:
            query = {'issue_type': issuetype}

        context = aq_inner(self.context)
        return self.portal_catalog(query,
                        object_provides=ITicket.__identifier__,
                        path='/'.join(context.getPhysicalPath()),
                        sort_on='sortable_title')

    def showingState(self):
        #TODO i18n this
        if 'state' in self.request:
            states = self.getReviewStates()
            for x in states:
                if x.id == self.request.state:
                    return x.title
        return "All"

    def privateDataAllowed(self):
        if getSecurityManager().checkPermission(
                                    'fourdigits.tracker: View Private Data',
                                    aq_inner(self.context)):
            return True
        else:
            return False
