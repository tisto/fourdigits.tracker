from five import grok
from Products.CMFCore.utils import getToolByName
from fourdigits.tracker.tracker import ITracker
from fourdigits.tracker.ticket import ITicket
from datetime import date, datetime, time, timedelta
from plone.directives import form
from zope.interface import alsoProvides
from zope import schema
from plone.z3cform.interfaces import IWrappedForm
from Products.CMFCore.interfaces import ISiteRoot
from z3c.form import button
from fourdigits.tracker import MessageFactory as _

grok.templatedir("templates")


class IPeriodForm(form.Schema):
    begin = schema.Date(title=_(u"begin"))
    end = schema.Date(title=_(u"end"))


class PeriodForm(form.SchemaForm):
    schema = IPeriodForm
    grok.context(ISiteRoot)
    ignoreContext = True

    @button.buttonAndHandler(_('Submit'), name='submit')
    def handle_submit(self, action):
        data, errors = self.extractData()
        if errors:
            self.status = "Please correct errors"
            return


class TimeView(grok.View):
    grok.context(ITracker)
    grok.require('zope2.View')
    grok.name('timeview')

    beginDate = "empty"
    endDate = "empty"

    @property
    def portal_catalog(self):
        return getToolByName(self.context, 'portal_catalog')

    @property
    def workflowTool(self):
        return getToolByName(self.context, "portal_workflow")

    def timeSpentInPeriod(self):

        # Check if the form is submitted, and all fields are filled
        if ('form.buttons.submit' not in self.request.form) or \
            (self.request.form.get('form.widgets.begin-day') == u'') or \
                (self.request.form.get('form.widgets.end-day') == u''):

            endDate = datetime.combine(date.today(), time(23, 59, 59))
            beginDate = endDate - timedelta(days=8)
        else:
            beginDate, endDate = self.getFormDates()

        self.beginDate = beginDate
        self.endDate = endDate
        query = {}
        query['modified'] = {"query": [beginDate, endDate],
                                         "range": "minmax"}
        brains = self.portal_catalog(query,
                        object_provides=ITicket.__identifier__,
                        path='/'.join(self.context.getPhysicalPath()),
                        sort_on='sortable_title')
        return self.calculateTimeSpent(brains)

    def getFormDates(self):
        request = self.request

        # Create a string from the form date
        formBeginDate = "%s-%s-%s" % (request.get('form.widgets.begin-year'),
            request.get('form.widgets.begin-month'),
            request.get('form.widgets.begin-day'))

        beginDate = datetime.strptime("%s 00:00:01" % formBeginDate,
            "%Y-%m-%d %H:%M:%S")

        formEndDate = "%s-%s-%s" % (request.get('form.widgets.end-year'),
            request.get('form.widgets.end-month'),
            request.get('form.widgets.end-day'))

        endDate = datetime.strptime("%s 23:59:59" % formEndDate,
            "%Y-%m-%d %H:%M:%S")

        return beginDate, endDate

    def calculateTimeSpent(self, brainTickets):
        # TODO Get the time spent in the ticket
        totals = {}
        if len(brainTickets) == 0:
            return totals
        states, workflow_name =\
            self.getAvailableStates(brainTickets[0].getObject())

        for state in states:
            totals[state] = 0

        for brain in brainTickets:
            obj = brain.getObject()
            state = self.workflowTool.getStatusOf(workflow_name,
                obj)['review_state']
            timeFromChildren = self.getTimeFromChildren(obj)
            totals[state] = totals[state] + timeFromChildren

        totalTime = 0.0
        for key, value in totals.iteritems():
            totalTime = totalTime + value
        totals["total"] = totalTime

        return totals

    def getTimeFromChildren(self, obj):
        total = 0
        for childID in obj:
            child = obj[childID]
            modDate = datetime.fromtimestamp(child.modification_date)
            if self.beginDate < modDate and modDate < self.endDate:
                total = total + child.time_spent
        return total

    def getAvailableStates(self, obj):
        workflow_name = self.workflowTool.getChainFor(obj)[0]
        workflow = self.workflowTool.\
            getWorkflowById(workflow_name)
        states = []
        for state in workflow.states:
            states.append(state)

        return states, workflow_name

    def getForm(self):
        self.form = PeriodForm(self.context, self.request)
        alsoProvides(self.form, IWrappedForm)
        self.form.update()
        return self.form()

    def setForm(self):
        self.form = self.form(self.context, self.request)
        alsoProvides(self.form, IWrappedForm)
        self.form.update()
