from five import grok
from zope.publisher.interfaces import IPublishTraverse
from Products.CMFCore.utils import getToolByName
from fourdigits.tracker.ticket import ITicket
from Products.Five import BrowserView


class TicketTraverser(BrowserView):
    grok.implements(IPublishTraverse)

    def __call__(self):
        return self.traverse()

    def traverse(self):
        if getattr(self, 'trav', None):
            portal_catalog = getToolByName(self.context, 'portal_catalog')
            query = {'object_provides': ITicket.__identifier__,
                        'id': self.trav}
            result = portal_catalog.search(query)

            if result:
                url = result[0].getURL()
                self.request.response.redirect(url)
            else:
                return "No ticket found"
        else:
            return "No ticket number"

    def publishTraverse(self, request, name):
        self.trav = name
        return self()
