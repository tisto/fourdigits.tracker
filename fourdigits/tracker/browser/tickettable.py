from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import random


class TicketTable(BrowserView):

    template = ViewPageTemplateFile("templates/tickettable.pt")

    def __call__(self, tickets):
        self.tickets = tickets
        return self.template()

    def getRandomID(self):
        #This random ID is for the sorting of the table as described in
        #http://plone.org/documentation/kb/making-tables-sortable/usage
        return random.randrange(0, 100000000)
