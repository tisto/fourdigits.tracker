from five import grok
from plone.directives import dexterity, form
from zope import schema
from fourdigits.tracker import MessageFactory as _
from plone.namedfile.field import NamedBlobFile
from plone.uuid.interfaces import IAttributeUUID


class ITicketResponse(form.Schema):
    """
    TicketResponse Type
    """

    responsetype = schema.Choice(
            title=_(u"Response type"),
            vocabulary=u"fourdigits.tracker.vocabularies.responsetypes",
            required=False,
            default=u'normal',
            )

    dexterity.write_permission(time_spent='cmf.ManagePortal')
    dexterity.read_permission(time_spent='cmf.ManagePortal')
    attachments = NamedBlobFile(
            title=_(u"Attachments"),
            required=False,
            )

    time_spent = schema.Float(
            title=_(u"Time spent"),
            required=True,
            )


class TicketResponse(dexterity.Item):
    grok.implements(ITicketResponse, IAttributeUUID)
