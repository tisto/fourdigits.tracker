from five import grok
from fourdigits.tracker import ticket


class IPrivateTicket(ticket.ITicket):
    """
    Private Ticket Type
    """


class PrivateTicket(ticket.Ticket):
    grok.implements(IPrivateTicket)
