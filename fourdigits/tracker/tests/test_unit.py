import unittest2 as unittest
from fourdigits.tracker.testing import INTEGRATION_TESTING

from zope.component import getUtility

from plone.dexterity.utils import createContentInContainer
from zope.app.container.interfaces import INameChooser
from fourdigits.tracker.namechooser import TrackerNameChooser, \
                                            TicketNameChooser
from zExceptions import BadRequest
from random import choice

from fourdigits.tracker.globalint import IGlobalInt
from fourdigits.tracker.traversing import ticket as ticket_traverser


class GlobalintTest(unittest.TestCase):

    layer = INTEGRATION_TESTING

    # Test if we have globalint utility is sequential
    def test_globalint_sequential(self):
        globalint = getUtility(IGlobalInt)
        ints = []
        for x in range(6):
            int_ = globalint()
            ints.append(int_)
        self.assertEqual(ints, [1, 2, 3, 4, 5, 6, ])

    # Test if we have multiple globalint sequenses
    def test_globalint_multiple(self):
        globalint = getUtility(IGlobalInt)

        sequence1 = []
        sequence1.append(globalint(name='sequence1'))
        sequence1.append(globalint(name='sequence1'))
        sequence1.append(globalint(name='sequence1'))

        sequence2 = []
        globalint(name='sequence2')
        sequence2.append(globalint(name='sequence2'))
        sequence2.append(globalint(name='sequence2'))
        sequence2.append(globalint(name='sequence2'))

        sequence3 = []
        globalint(name='sequence3')
        globalint(name='sequence3')
        sequence3.append(globalint(name='sequence3'))
        sequence3.append(globalint(name='sequence3'))
        sequence3.append(globalint(name='sequence3'))

        self.assertEqual(sequence1, [1, 2, 3])
        self.assertEqual(sequence2, [2, 3, 4])
        self.assertEqual(sequence3, [3, 4, 5])


class NamechooserTest(unittest.TestCase):

    layer = INTEGRATION_TESTING

    # Test if the IntNameChooser is used for objects inside
    # a tracker, ticket and privateticket
    def test_namechooser_assignment(self):
        portal = self.layer['portal']
        container = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)

        namechooser = INameChooser(container)
        self.assertEqual(namechooser.__class__, TrackerNameChooser)

        container = createContentInContainer(
                        portal,
                        'fourdigits.tracker.ticket',
                        title=u'My ticket',
                        checkConstraints=False)
        namechooser = INameChooser(container)
        self.assertEqual(namechooser.__class__, TicketNameChooser)

        container = createContentInContainer(
                        portal,
                        'fourdigits.tracker.privateticket',
                        title=u'My private ticket',
                        checkConstraints=False)
        namechooser = INameChooser(container)
        self.assertEqual(namechooser.__class__, TicketNameChooser)

    # Test if the namechooser returns int wrapped as str,
    # as OFS ObjectManager doesn't do ints
    def test_namechooser_str(self):
        portal = self.layer['portal']
        container = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)
        namechooser = INameChooser(container)

        ob = createContentInContainer(
                container,
                'fourdigits.tracker.ticket',
                title=u"My ticket",
                checkConstraints=False)
        id_ = namechooser.chooseName(None, ob)
        # is it a str
        self.assertTrue(isinstance(id_, str))

        # but is it an int?
        self.assertTrue(isinstance(int(id_), int))

    # Test if the ids are sequential
    def test_sequential_ints(self):
        portal = self.layer['portal']
        # Setup a tracker
        tracker = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)

        # Create tickets
        ids = []
        for x in range(6):
            ob = createContentInContainer(
                    tracker,
                    'fourdigits.tracker.ticket',
                    title=u"My ticket",
                    checkConstraints=False)
            ids.append(ob.id)
        self.assertEqual(ids, ['1', '2', '3', '4', '5', '6'])

    # Test if the ids are sequential over multiple trackers
    def test_sequential_ints_multiple(self):
        portal = self.layer['portal']

        # Setup multiple trackers
        trackers = []
        for x in range(6):
            tracker = createContentInContainer(
                            portal,
                            'fourdigits.tracker.tracker',
                            title=u'My tracker',
                            checkConstraints=False)
            trackers.append(tracker)

        # Create tickets
        ids = []
        for x in range(99):
            tracker = choice(trackers)
            ob = createContentInContainer(
                    tracker,
                    'fourdigits.tracker.ticket',
                    title=u"My ticket",
                    checkConstraints=False)
            ids.append(ob.id)

        # convert ids to int for sorting
        ids = map(int, ids)

        # Make sure every ticket has an unique id
        self.assertEqual(len(ids), len(set(ids)))

        # Make sure the ids are sequential
        self.assertEqual(ids, sorted(ids))

    # Test duplicate ids
    def test_duplicates(self):
        portal = self.layer['portal']

        # Setup a tracker
        tracker1 = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)

        # Create duplicate tickets int one tracker
        duplicates_caught = False
        try:
            tracker1.invokeFactory('fourdigits.tracker.ticket', id='1')
            tracker1.invokeFactory('fourdigits.tracker.ticket', id='1')
        except BadRequest:
            duplicates_caught = True
        self.assertTrue(duplicates_caught)

        # Create duplicate tickets in multiple tracker
        # This doesn't work yet. We need to add some event to hook into
        # objectadded and raise 'duplicate id' there.
        # No plans to fix this any time soon
        """
        tracker2 = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)

        duplicates_caught = False
        try:
            tracker1.invokeFactory('fourdigits.tracker.ticket', id='2')
            tracker2.invokeFactory('fourdigits.tracker.ticket', id='2')
        except BadRequest:
            duplicates_caught = True
        self.assertTrue(duplicates_caught)
        """

    # Test non-int ids
    def test_nonint(self):
        # This doesn't work yet. We need to add some event to hook into
        # objectadded and raise 'invalid id' there.
        # No plans to fix this any time soon
        """
        portal = self.layer['portal']

        # Setup a tracker
        tracker = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)

        # Check for non-int id
        caught = False
        try:
            tracker.invokeFactory('fourdigits.tracker.ticket', id='abcdef')
        except:
            caught = True
        self.assertTrue(caught)
        """

    # Test if ticketresponses have an int sequence within the ticket
    def test_response(self):
        portal = self.layer['portal']

        # Setup a trackers
        tracker = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)

        # Add a ticket
        ticket1 = createContentInContainer(
                        tracker,
                        'fourdigits.tracker.ticket',
                        title=u'My ticket',
                        checkConstraints=False)

        # Add another ticket to up the sequence
        ticket2 = createContentInContainer(
                        tracker,
                        'fourdigits.tracker.ticket',
                        title=u'My ticket',
                        checkConstraints=False)

        # Add several responses to both tickets
        for x in range(4):
            createContentInContainer(
                        ticket1,
                        'fourdigits.tracker.ticketresponse',
                        title=u'My respone %d in tracker 1' % x,
                        checkConstraints=False)
        for x in range(4):
            createContentInContainer(
                        ticket2,
                        'fourdigits.tracker.ticketresponse',
                        title=u'My respone %d in tracker 2' % x,
                        checkConstraints=False)

        self.assertEqual(ticket1.objectIds(), ['1', '2', '3', '4', ])
        self.assertEqual(ticket2.objectIds(), ['1', '2', '3', '4', ])


# Test the ticket traverser
class TraverserTest(unittest.TestCase):

    layer = INTEGRATION_TESTING

    # Test the ++ticket++%d format for tickets
    def test_ticket_only(self):
        portal = self.layer['portal']

        # Setup a trackers
        tracker = createContentInContainer(
                        portal,
                        'fourdigits.tracker.tracker',
                        title=u'My tracker',
                        checkConstraints=False)

        # Setup a ticket
        ticket = createContentInContainer(
                        tracker,
                        'fourdigits.tracker.ticket',
                        title=u'My ticket',
                        checkConstraints=False)

        # Add some responses
        for x in range(6):
            response = createContentInContainer(
                            ticket,
                            'fourdigits.tracker.ticketresponse',
                            title=u'My respone %d' % x,
                            checkConstraints=False)

        # setup traverser
        traverser = ticket_traverser()
        traverser.context = portal
        traverser.request = portal.REQUEST

        # Test if we get the correct url for tickets
        url = traverser.getUrl(ticket.id)
        wanted = 'http://nohost/plone/my-tracker/%s' % ticket.id
        self.assertEqual(wanted, url)

        # Test if we get redirected to the response's anchor
        url = traverser.getUrl('%s#%s' % (ticket.id, response.id))
        wanted = 'http://nohost/plone/my-tracker/%s#response-id-%s'
        wanted = wanted % (ticket.id, response.id)
        self.assertEqual(wanted, url)
