from five import grok

from zope.component import getUtility

from zope.app.container.contained import NameChooser
from zope.app.container.interfaces import INameChooser

from fourdigits.tracker.globalint import IGlobalInt

from fourdigits.tracker.ticket import ITicket
from fourdigits.tracker.tracker import ITracker


# Give tickets an intid
class TrackerNameChooser(grok.Adapter, NameChooser):
    grok.provides(INameChooser)
    grok.context(ITracker)

    def chooseName(self, name, ob):
        globalint = getUtility(IGlobalInt)
        next_int = globalint()
        return str(next_int)


# Give ticketresponses an intid, but different from ticketresponseset ids
class TicketNameChooser(grok.Adapter, NameChooser):
    grok.provides(INameChooser)
    grok.context(ITicket)

    def chooseName(self, name, ob):
        globalint = getUtility(IGlobalInt)
        next_int = globalint(name='responses_tracker_%s' % self.context.id)
        return str(next_int)
