from zope import schema
from zope.interface import Interface

from zope.i18nmessageid import MessageFactory

from plone.app.registry.browser import controlpanel
from five import grok
from Products.CMFPlone.interfaces import IPloneSiteRoot
from plone.z3cform.textlines.textlines import TextLinesFieldWidget

_ = MessageFactory('fourdigits.tracker')


class ITrackerSettings(Interface):
    enabled = schema.Bool(
        title=u"Commitparser enabled",
    )

    authkey = schema.TextLine(
        title=u'Commitparser authkey',
        default=u"",
        required=False,
    )

    gozerbotenabled = schema.Bool(
        title=u"Gozerbot via udp plugin enabled",
    )

    gozerbothost = schema.TextLine(
        title=u'Gozerbot host',
        default=u"",
        required=False,
    )

    gozerbotpassword = schema.TextLine(
        title=u'Gozerbot password',
        default=u"",
        required=False,
    )

    gozerbotchannel = schema.TextLine(
        title=u'Gozerbot channel, including #',
        default=u"",
        required=False,
    )

    gozerbotport = schema.TextLine(
        title=u'Gozerbot port',
        default=u"",
        required=False,
    )


class TrackerSettingsEditForm(controlpanel.RegistryEditForm):
    schema = ITrackerSettings
    label = _(u"Tracker settings")
    description = _(u"Settings for Four Digits Tracker")


class TrackerSettingsControlPanel(controlpanel.ControlPanelFormWrapper,
                                    grok.View):
    grok.context(IPloneSiteRoot)
    grok.name('tracker-settings')
    form = TrackerSettingsEditForm
