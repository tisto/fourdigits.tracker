from five import grok
from fourdigits.tracker import MessageFactory as _
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from zope.app.component.hooks import getSite
from zope.schema.interfaces import IVocabularyFactory
from zope.app.component import hooks


class followers(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        terms = SimpleVocabulary([
            SimpleTerm(value=u'henk', title=u'Henk'),
            SimpleTerm(value=u'jos', title=u'Jos'),
            SimpleTerm(value=u'piet', title=u'Piet'),
        ])
        return terms


class employees(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        site = hooks.getSite()
        groupsTool = site.portal_groups
        group = groupsTool.getGroupById("employees")

        result = []
        if group:
            result = group.getGroupMembers()

        terms = []
        for member in result:
            terms.append(SimpleVocabulary.createTerm(
                                        member.getUserName(),
                                        str(member.getUserName()),
                                        member.getProperty('fullname')))
        return SimpleVocabulary(terms)


class priorities(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        terms = SimpleVocabulary([
            SimpleTerm(value=u'fatal', title=u'Fatal'),
            SimpleTerm(value=u'high', title=u'High'),
            SimpleTerm(value=u'middle', title=u'Middle'),
            SimpleTerm(value=u'low', title=u'Low'),
        ])
        return terms


class trackertypes(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        terms = SimpleVocabulary([
            SimpleTerm(value=u'empty', title=_(u'Empty project')),
            SimpleTerm(value=u'support', title=_(u'Support')),
            SimpleTerm(value=u'project', title=_(u'Project')),
        ])
        return terms


class issuetypes(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        terms = SimpleVocabulary([
            SimpleTerm(value=u'bug', title=_(u'Bug')),
            SimpleTerm(value=u'werkzaamheid', title=_(u'Werkzaamheid')),
            SimpleTerm(value=u'preventieve_werkzaamheid',
                        title=_(u'Preventieve werkzaamheid')),
            SimpleTerm(value=u'incident', title=_(u'Incident')),
            SimpleTerm(value=u'change', title=_(u'Change')),
        ])
        return terms


class responsetypes(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        terms = SimpleVocabulary([
            SimpleTerm(value=u'vcs', title=_(u'VCS message')),
            SimpleTerm(value=u'system', title=_(u'System message')),
            SimpleTerm(value=u'normal', title=_(u'Normal response')),
        ])
        return terms


class members(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        query = {}
        context = getSite()
        result = context.portal_membership.searchForMembers()
        terms = []
        for member in result:
            terms.append(SimpleVocabulary.createTerm(
                                        member.getUserName(),
                                        str(member.getUserName()),
                                        member.getProperty('fullname')))
        return SimpleVocabulary(terms)


grok.global_utility(priorities,
    name=u"fourdigits.tracker.vocabularies.priorities")

grok.global_utility(employees,
    name=u"fourdigits.tracker.vocabularies.employees")

grok.global_utility(followers,
    name=u"fourdigits.tracker.vocabularies.followers")

grok.global_utility(trackertypes,
    name=u"fourdigits.tracker.vocabularies.trackertypes")

grok.global_utility(issuetypes,
    name=u"fourdigits.tracker.vocabularies.issuetypes")

grok.global_utility(responsetypes,
    name=u"fourdigits.tracker.vocabularies.responsetypes")

grok.global_utility(members,
    name=u"fourdigits.tracker.vocabularies.members")
