from zope.configuration import xmlconfig

from plone.app.testing import (PloneSandboxLayer,
                               FunctionalTesting,
                               PLONE_FIXTURE,
                               IntegrationTesting,
                               TEST_USER_NAME,
                               TEST_USER_ID,
                               login,
                               setRoles, )

from plone.testing.z2 import ZSERVER_FIXTURE


class FourdigitsTracker(PloneSandboxLayer):
    defaultBases = (PLONE_FIXTURE, )

    def setUpZope(self, app, configurationContext):
        import fourdigits.tracker
        xmlconfig.file('configure.zcml', fourdigits.tracker,
                       context=configurationContext)
        self.loadZCML(package=fourdigits.tracker)

    def setUpPloneSite(self, portal):
        self.applyProfile(portal, 'fourdigits.tracker:default')
        setRoles(portal, TEST_USER_ID, ['Manager'])
        login(portal, TEST_USER_NAME)


MY_PACKAGE_FIXTURE = FourdigitsTracker()

MY_PACKAGE_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(MY_PACKAGE_FIXTURE, ZSERVER_FIXTURE),
    name="TRACKER:Acceptance")

FIXTURE = FourdigitsTracker()
INTEGRATION_TESTING = IntegrationTesting(bases=(FIXTURE, ),
                                         name="fourdigits.tracker:Integration")
