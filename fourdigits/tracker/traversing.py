from five import grok

from zope.location.interfaces import LocationError
from zope.traversing.interfaces import ITraversable
from zope.interface.interfaces import IInterface
from zope.publisher.interfaces import IRequest

from Products.CMFCore.utils import getToolByName

from fourdigits.tracker.ticket import ITicket


class ticket(grok.MultiAdapter):
    grok.provides(ITraversable)
    grok.adapts(IInterface, IRequest)

    def traverse(self, name, ignored):
        url = self.getUrl(name)
        if not url:
            raise LocationError(self.context, "++ticket++%s" % (name))
        self.request.response.redirect(url)

    def getUrl(self, name):
        if not '#' in name:
            return self.getTicketUrl(name)

        ticket_id, response_id = name.split('#')
        url = self.getTicketUrl(ticket_id)
        return "%s#response-id-%s" % (url, response_id)

    # get url for ticket
    def getTicketUrl(self, ticket_id):
        catalog = getToolByName(self.context, 'portal_catalog')
        query = {'object_provides': ITicket.__identifier__,
                'id': str(ticket_id), }
        brains = catalog.unrestrictedSearchResults(query)
        if not brains:
            return
        return brains[0].getURL()
