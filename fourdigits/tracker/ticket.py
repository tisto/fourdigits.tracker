from five import grok
from plone.directives import dexterity, form
from zope import schema
from fourdigits.tracker import MessageFactory as _
from plone.namedfile.field import NamedBlobFile
from plone.uuid.interfaces import IAttributeUUID

from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary
from Acquisition import aq_parent, aq_inner
from plone.indexer import indexer
from zope.component import getUtility
from zope.schema.interfaces import IVocabularyFactory
from AccessControl import getSecurityManager


@grok.provider(IContextSourceBinder)
def possiblePriority(context):
    terms = []
    tracker = context
    if tracker.portal_type != 'fourdigits.tracker.tracker':
        tracker = aq_parent(context)
    for x in tracker.priorities or []:
        terms.append(SimpleVocabulary.createTerm(x, str(x), x.title()))
    return SimpleVocabulary(terms)


@grok.provider(IContextSourceBinder)
def possibleIssueType(context):
    terms = []
    tracker = context
    if tracker.portal_type != 'fourdigits.tracker.tracker':
        tracker = aq_parent(context)
    for x in tracker.issuetypes or []:
        terms.append(SimpleVocabulary.createTerm(x, str(x), x.title()))
    return SimpleVocabulary(terms)


@grok.provider(IContextSourceBinder)
def getManagers(context):
    """
    """
    tracker = context
    if tracker.portal_type != 'fourdigits.tracker.tracker':
        tracker = aq_parent(context)
    items = tracker.managers
    vocab = []
    vocab.append(SimpleVocabulary.createTerm('(UNASSIGNED)',
                                            _(u'(UNASSIGNED)'),
                                             '(UNASSIGNED)'))

    # Check if there are local set managers and add them
    if items:
        for x in items:
            vocab.append(SimpleVocabulary.createTerm(x, str(x), x.title()))

    # Get the employees as they should always be managers
    factory = getUtility(IVocabularyFactory,
                         'fourdigits.tracker.vocabularies.employees')
    employees = factory(context)
    if employees:
        for employee in employees:
            vocab.append(employee)

    return SimpleVocabulary(vocab)


class ITicket(form.Schema):
    """
    Ticket Type
    """
    priority = schema.Choice(
            title=_(u"Priority"),
            source=possiblePriority,
            required=False,
        )
    details = schema.Text(
            title=_(u"Details"),
            required=False,
        )
    url = schema.TextLine(
            title=_(u"URL"),
            required=False,
        )
    issue_type = schema.Choice(
            title=_(u"Issue type"),
            source=possibleIssueType,
            required=False,
        )
    responsiblemanager = schema.Choice(
            title=_(u"Responsible manager"),
            source=getManagers,
            default="(UNASSIGNED)",
            required=False,
        )
    followers = schema.Choice(
            title=_(u"Followers"),
            vocabulary=u"fourdigits.tracker.vocabularies.followers",
            required=False,
        )

    form.write_permission(time_estimate='cmf.ManagePortal')
    form.read_permission(time_estimate='cmf.ManagePortal')
    time_estimate = schema.Float(
            title=_(u"Time estimate"),
            required=True,
            default=float(0),
        )

    # Computed for display
    time_real = schema.Float(
            title=_(u"Time real"),
            required=False,
            default=float(0),
        )
    form.omitted("time_real")
    deadline = schema.Date(
            title=_(u"Deadline"),
            required=False,
        )
    attachments = NamedBlobFile(
            title=_(u"Attachments"),
            required=False,
        )
    private_text = schema.Text(
            title=_(u"Private text"),
            required=False,
        )

# Custom content-type class; objects created for this content type will
# be instances of this class. Use this class to add content-type specific
# methods and properties. Put methods that are mainly useful for rendering
# in separate view classes.


class AddForm(dexterity.AddForm):
    grok.name('fourdigits.tracker.ticket')

    def updateWidgets(self):
        super(AddForm, self).updateWidgets()
        self.widgets['details'].rows = 20
        self.widgets['IBasic.description'].mode = 'hidden'


class EditForm(dexterity.EditForm):
    grok.context(ITicket)

    def updateWidgets(self):
        super(EditForm, self).updateWidgets()
        self.widgets['details'].rows = 20
        self.widgets['IBasic.description'].mode = 'hidden'


class Ticket(dexterity.Container):
    grok.implements(ITicket, IAttributeUUID)

    def canSetDefaultPage(self):
        return False
    # Add your class methods and properties here

    def privateDataAllowed(self):
        if getSecurityManager().checkPermission(
                                    'fourdigits.tracker: View Private Data',
                                    aq_inner(self)):
            return True
        else:
            return False


@indexer(ITicket)
def issue_typeIndexer(ob):
    return ob.issue_type
grok.global_adapter(issue_typeIndexer, name="issue_type")


@indexer(ITicket)
def responsiblemanagerIndexer(ob):
    return ob.responsiblemanager
grok.global_adapter(responsiblemanagerIndexer, name="responsiblemanager")
