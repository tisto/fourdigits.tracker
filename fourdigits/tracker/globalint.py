from five import grok
from zope.interface import Interface
from zope.app.component import hooks
from zope.annotation import IAnnotations


class IGlobalInt(Interface):
    pass


class globalint(object):
    grok.implements(IGlobalInt)

    def __call__(self, name=None):
        site = hooks.getSite()
        annotations = IAnnotations(site)
        key = 'tracker-global-counter'
        if name:
            key = '%s-%s' % (key, str(name))
        counter = annotations.get(key, 0)
        annotations[key] = counter + 1
        return annotations[key]

    def set(self, value, name=None):
        site = hooks.getSite()
        annotations = IAnnotations(site)
        key = 'tracker-global-counter'
        if name:
            key = '%s-%s' % (key, str(name))
        annotations[key] = value
        return annotations[key]

    def get(self, name=None):
        site = hooks.getSite()
        annotations = IAnnotations(site)
        key = 'tracker-global-counter'
        if name:
            key = '%s-%s' % (key, str(name))
        return annotations[key]

grok.global_utility(globalint)
