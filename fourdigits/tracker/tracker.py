from five import grok
from plone.directives import dexterity, form
from zope import schema
#from z3c.form import group, field
#from plone.app.z3cform.wysiwyg import WysiwygFieldWidget
from fourdigits.tracker import MessageFactory as _

from plone.uuid.interfaces import IAttributeUUID
from plone.indexer import indexer


class ITracker(form.Schema):
    """
    Tracker Type
    """
    trackertype = schema.Choice(
            title=_(u"Tracker type"),
            vocabulary=u"fourdigits.tracker.vocabularies.trackertypes",
            required=False,
        )
    projectmanager = schema.Choice(
            title=_(u"Project manager"),
            vocabulary=u"fourdigits.tracker.vocabularies.employees",
            required=False,
        )
    managers = schema.List(
            title=_(u"Managers"),
            value_type=schema.Choice(
                vocabulary=u"fourdigits.tracker.vocabularies.members",
            ),
            required=False,
        )
    priorities = schema.List(
            title=_(u"Priorities"),
            value_type=schema.Choice(
                vocabulary=u"fourdigits.tracker.vocabularies.priorities",
                ),
            required=False,
        )
    issuetypes = schema.List(
            title=_(u"Issue types"),
            value_type=schema.Choice(
                vocabulary=u"fourdigits.tracker.vocabularies.issuetypes",
            ),
            required=False,
        )
    helptext = schema.Text(
            title=_(u"Help text"),
            required=False,
        )


class Tracker(dexterity.Container):
    grok.implements(ITracker, IAttributeUUID)

    def canSetDefaultPage(self):
        return False


@indexer(ITracker)
def projectmanagerIndexer(ob):
    return ob.projectmanager
grok.global_adapter(projectmanagerIndexer, name="projectmanager")
