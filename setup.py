from setuptools import setup, find_packages
import os

version = '0.1'

setup(name='fourdigits.tracker',
          version=version,
          description="Four Digits Tracker",
          long_description=open("README.rst").read() + "\n" +
                           open("CHANGES.rst").read(),
        classifiers=[
            "Framework :: Plone",
            "Programming Language :: Python",
            "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='',
      author='Four Digits',
      author_email='info@fourdigits.nl',
      url='http://www.fourdigits.nl',
      license='GPL version 2',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['fourdigits'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'plone.app.dexterity',
          'plone.app.versioningbehavior',
          'plone.app.testing',
          'five.grok',
          'plone.namedfile',
          'plone.namedfile[blobs]',
          'plone.formwidget.namedfile',
          'robotsuite',
          'robotframework-selenium2library',
          'PIL',
          # -*- Extra requirements: -*-
      ],
      extras_require={
        'test': [
          'plone.app.testing',
        ],
      },
      entry_points="""
      # -*- Entry points: -*-
      [z3c.autoinclude.plugin]
      target = plone
      """,
      setup_requires=["PasteScript"],
      paster_plugins=["ZopeSkel"],

      )
